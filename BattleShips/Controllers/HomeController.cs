﻿using Lab7.Models;
using Lab7.Services;
using System.Web.Mvc;

namespace Lab7.Controllers
{
    public class HomeController : Controller
    {
        private readonly DatabaseService _dataBaseService;
        public HomeController()
        {
            _dataBaseService = new DatabaseService();
           
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Start(Game game)
        {
            if (!_dataBaseService.FoundGame(game.FirstPlayer.Name, game.SecondPlayer.Name))
            {
                _dataBaseService.CreateGame(game.FirstPlayer.Name, game.SecondPlayer.Name, 6, 6);
            }

            var newGame = _dataBaseService.GetGame(game.FirstPlayer.Name, game.SecondPlayer.Name);

            return View("Play", newGame);
        }
        [HttpPost]
        public ActionResult Play(Game game)
        {
            var newGame = new Game();
            if (_dataBaseService.Shoot(game.FirstPlayer.Name, game.SecondPlayer.Name, game.XSize, game.YSize))
            {
                newGame = _dataBaseService.GetGame(game.FirstPlayer.Name, game.SecondPlayer.Name);
            }
            return View("Play", newGame);
        }
    }
}