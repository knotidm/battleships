﻿using MongoDB.Bson.Serialization.Attributes;

namespace Lab7.Models
{
    [BsonIgnoreExtraElements]
    public class Player
    {
        [BsonElement("name")]
        public string Name { get; set; }
    }
}