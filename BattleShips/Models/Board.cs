﻿using System.Collections.Generic;

namespace Lab7.Models
{
    public class Board
    {
        public ICollection<Cell> Cells { get; private set; }
        public Board()
        {
            Cells = new List<Cell>();
        }
    }
}
