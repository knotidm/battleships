﻿using MongoDB.Bson.Serialization.Attributes;

namespace Lab7.Models
{
    [BsonIgnoreExtraElements]
    public class Cell
    {
        [BsonId]
        [BsonElement("_id")]
        public string Id { get; set; }
        [BsonElement("x")]
        public int X { get; set; }
        [BsonElement("y")]
        public int Y { get; set; }
        [BsonElement("currentCellState")]
        public int CurrentCellState { get; set; }
        
    }
}