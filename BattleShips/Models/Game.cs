﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Lab7.Models
{
    [BsonIgnoreExtraElements]
    public class Game
    {
        [BsonId]
        [BsonElement("_id")]
        public string Id { get; set; }
        [BsonElement("firstPlayer")]
        public Player FirstPlayer { get; set; }
        [BsonElement("firstPlayerBoard")]
        public Board FirstPlayerBoard { get; set; }
        [BsonElement("secondPlayer")]
        public Player SecondPlayer { get; set; }
        [BsonElement("secondPlayerBoard")]
        public Board SecondPlayerBoard { get; set; }
        [BsonElement("currentPlayer")]
        public int CurrentPlayer { get; set; }
        [BsonElement("xSize")]
        public int XSize { get; set; }
        [BsonElement("ySize")]
        public int YSize { get; set; }
    }
}
