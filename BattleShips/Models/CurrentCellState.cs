﻿namespace Lab7.Models
{
    public static class CurrentCellState
    {
        public const int Empty = 0;
        public const int Ship = 1;
        public const int Hitted = 2;
        public const int Mishit = 3;
    }
}