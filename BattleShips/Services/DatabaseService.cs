﻿using Lab7.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;

namespace Lab7.Services
{
    public class DatabaseService
    {
        readonly IMongoCollection<Game> _gameCollection;
        private readonly IMongoCollection<Player> _playerCollection;
        private readonly IMongoCollection<Cell> _cellCollection;
        public DatabaseService()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("Ships");     
            _gameCollection = database.GetCollection<Game>("Game");
            _playerCollection = database.GetCollection<Player>("Player");
            _cellCollection = database.GetCollection<Cell>("Cell");
        }
        public bool FoundGame(string firstPlayerName, string secondPlayerName)
        {
            return _gameCollection.AsQueryable().Any(g => g.FirstPlayer.Name == firstPlayerName && g.SecondPlayer.Name == secondPlayerName);
        }
        public Game GetGame(string firstPlayerName, string secondPlayerName)
        {
            var game = _gameCollection.AsQueryable().FirstOrDefault(g => g.FirstPlayer.Name == firstPlayerName && g.SecondPlayer.Name == secondPlayerName);
            return game;
        }
        public void CreateGame(string firstPlayerName, string secondPlayerName, int xSize, int ySize)
        {
            var player1 = !FoundPlayer(firstPlayerName) ? 
                new Player() { Name = firstPlayerName } : 
                GetPlayer(firstPlayerName);

            var player2 = !FoundPlayer(secondPlayerName) ? 
                new Player() { Name = secondPlayerName } : 
                new Player() { Name = secondPlayerName };

            var firstPlayerBoard = CreateBoard(xSize, ySize);
            var secondPlayerBoard = CreateBoard(xSize, ySize);

            _gameCollection.InsertOne(new Game()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                FirstPlayer = player1,
                SecondPlayer = player2,
                FirstPlayerBoard = firstPlayerBoard,
                SecondPlayerBoard = secondPlayerBoard,
                CurrentPlayer = CurrentPlayer.Firstplayer,
                XSize = xSize,
                YSize = ySize
            });
        }
        private bool GameUpdated(Game gameToUpdate)
        {
            try
            {
                var filter = Builders<Game>.Filter.Eq("_id", gameToUpdate.Id);
                _gameCollection.ReplaceOne(filter, gameToUpdate);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private bool FoundPlayer(string playerName)
        {
            return _playerCollection.AsQueryable().Any(p => p.Name == playerName);
        }
        private Player GetPlayer(string playerName)
        {
            return _playerCollection.AsQueryable().FirstOrDefault(p => p.Name == playerName);
        }
        public bool Shoot(string firstPlayerName, string secondPlayerName, int x, int y)
        {
            var game = GetGame(firstPlayerName, secondPlayerName);

            var cell = game.CurrentPlayer == CurrentPlayer.Firstplayer ? 
                game.SecondPlayerBoard.Cells.FirstOrDefault(c => c.X == x && c.Y == y) : 
                game.FirstPlayerBoard.Cells.FirstOrDefault(c => c.X == x && c.Y == y);
            
            if (cell != null && cell.CurrentCellState == CurrentCellState.Empty)
            {
                cell.CurrentCellState = CurrentCellState.Mishit;
            }
            else if (cell != null && (cell.CurrentCellState == CurrentCellState.Hitted || cell.CurrentCellState == CurrentCellState.Mishit))
            {
                return false;
            }
            else if (cell != null && cell.CurrentCellState == CurrentCellState.Ship)
            {
                cell.CurrentCellState = CurrentCellState.Hitted;
            }

            if (!CellUpdated(cell)) return false;
            var gameToUpdate = GetGame(firstPlayerName, secondPlayerName);
            gameToUpdate.CurrentPlayer = game.CurrentPlayer == CurrentPlayer.Firstplayer ? 
                CurrentPlayer.Secondplayer : 
                CurrentPlayer.Firstplayer;

            return GameUpdated(gameToUpdate);
        }
        private bool CellUpdated(Cell cellToUpdate)
        {
            try
            {
                var filter = Builders<Cell>.Filter.Eq("_id", cellToUpdate.Id);
                _cellCollection.ReplaceOne(filter, cellToUpdate);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static Board CreateBoard(int xSize, int ySize)
        {
            var createdBoard = new Board();
            
            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    createdBoard.Cells.Add(new Cell() {X = i, Y = j, CurrentCellState = CurrentCellState.Empty});
                }
            }

            decimal shipCount = xSize * ySize / 4;
            var rnd = new Random();

            for (int i = 0; i < Math.Floor(shipCount);i++)
            {
                var item = createdBoard.Cells.ElementAt(rnd.Next(createdBoard.Cells.Count()));
                if (item.CurrentCellState == CurrentCellState.Empty)
                {
                    item.CurrentCellState = CurrentCellState.Ship;
                }
                else i--;
            }
            return createdBoard;
        }
    }
}
